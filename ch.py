################################################################
# File: ch.py
# Title: Chatango Library
# Original Author: Lumirayz/Lumz <lumirayz@gmail.com>
# Maintainer: Dynamic/Daniel <d@duniel.no>
################################################################

################################################################
# License
################################################################
# Copyright 2011 Lumirayz
# This program is distributed under the terms of the GNU GPL.

# Imports
import re
import sys
import time
import queue
import select
import random
import asyncio
import aiohttp
import getpass
import websockets
import urllib.parse
import urllib.request

# Constants
# TODO: this has to go
Userlist_Recent = 0
Userlist_All = 1

BigMessage_Multiple = 0
BigMessage_Cut = 1

Number_of_Threads = 1

_HELLO_ALEC = {
	"origin": "https://st.chatango.com",
	"user-agent": "penis Mozilla/69 vagina Chrome/69 hi alec"
}

# Struct class
class Struct:
	def __init__(self, **entries):
		self.__dict__.update(entries)

# Tagserver stuff
specials = {'mitvcanal': 56, 'animeultimacom': 34, 'cricket365live': 21, 'pokemonepisodeorg': 22, 'animelinkz': 20, 'sport24lt': 56, 'narutowire': 10, 'watchanimeonn': 22, 'cricvid-hitcric-': 51, 'narutochatt': 70, 'leeplarp': 27, 'stream2watch3': 56, 'ttvsports': 56, 'ver-anime': 8, 'vipstand': 21, 'eafangames': 56, 'soccerjumbo': 21, 'myfoxdfw': 67, 'kiiiikiii': 21, 'de-livechat': 5, 'rgsmotrisport': 51, 'dbzepisodeorg': 10, 'watch-dragonball': 8, 'peliculas-flv': 69, 'tvanimefreak': 54, 'tvtvanimefreak': 54}
tsweights = [['5', 75], ['6', 75], ['7', 75], ['8', 75], ['16', 75], ['17', 75], ['18', 75], ['9', 95], ['11', 95], ['12', 95], ['13', 95], ['14', 95], ['15', 95], ['19', 110], ['23', 110], ['24', 110], ['25', 110], ['26', 110], ['28', 104], ['29', 104], ['30', 104], ['31', 104], ['32', 104], ['33', 104], ['35', 101], ['36', 101], ['37', 101], ['38', 101], ['39', 101], ['40', 101], ['41', 101], ['42', 101], ['43', 101], ['44', 101], ['45', 101], ['46', 101], ['47', 101], ['48', 101], ['49', 101], ['50', 101], ['52', 110], ['53', 110], ['55', 110], ['57', 110], ['58', 110], ['59', 110], ['60', 110], ['61', 110], ['62', 110], ['63', 110], ['64', 110], ['65', 110], ['66', 110], ['68', 95], ['71', 116], ['72', 116], ['73', 116], ['74', 116], ['75', 116], ['76', 116], ['77', 116], ['78', 116], ['79', 116], ['80', 116], ['81', 116], ['82', 116], ['83', 116], ['84', 116]]

def getServer(group):
	"""
	Get the server host for a certain room.

	@type group: str
	@param group: room name
	
	@rtype: str
	@return: the server's hostname
	"""
	try:
		sn = specials[group]
	except KeyError:
		group = group.replace("_", "q")
		group = group.replace("-", "q")
		fnv = float(int(group[0:min(5, len(group))], 36))
		lnv = group[6: (6 + min(3, len(group) - 5))]
		if(lnv):
			lnv = float(int(lnv, 36))
			lnv = max(lnv,1000)
		else:
			lnv = 1000
		num = (fnv % lnv) / lnv
		maxnum = sum(map(lambda x: x[1], tsweights))
		cumfreq = 0
		sn = 0
		for wgt in tsweights:
			cumfreq += float(wgt[1]) / maxnum
			if(num <= cumfreq):
				sn = int(wgt[0])
				break
	return "s" + str(sn) + ".chatango.com"

################################################################
# Uid
################################################################
def _genUid():
	"""
	generate a uid
	"""
	return str(random.randrange(10 ** 15, 10 ** 16 - 1))

################################################################
# Message stuff
################################################################
def _clean_message(msg):
	"""
	Clean a message and return the message, n tag and f tag.

	@type msg: str
	@param msg: the message

	@rtype: str, str, str
	@returns: cleaned message, n tag contents, f tag contents
	"""
	n = re.search("<n(.*?)/>", msg)
	if n: n = n.group(1)
	f = re.search("<f(.*?)>", msg)
	if f: f = f.group(1)
	msg = re.sub("<n.*?/>", "", msg)
	msg = re.sub("<f.*?>", "", msg)
	msg = _strip_html(msg)
	msg = msg.replace("&lt;", "<")
	msg = msg.replace("&gt;", ">")
	msg = msg.replace("&quot;", "\"")
	msg = msg.replace("&apos;", "'")
	msg = msg.replace("&amp;", "&")
	return msg, n, f

def _strip_html(msg):
	"""Strip HTML."""
	li = msg.split("<")
	if len(li) == 1:
		return li[0]
	else:
		ret = list()
		for data in li:
			data = data.split(">", 1)
			if len(data) == 1:
				ret.append(data[0])
			elif len(data) == 2:
				ret.append(data[1])
		return "".join(ret)

def _parseNameColor(n):
	"""This just returns its argument, should return the name color."""
	#probably is already the name
	return n

def _parseFont(f):
	"""Parses the contents of a f tag and returns color, face and size."""
	#' xSZCOL="FONT"
	try: #TODO: remove quick hack
		sizecolor, fontface = f.split("=", 1)
		sizecolor = sizecolor.strip()
		size = int(sizecolor[1:3])
		col = sizecolor[3:6]
		if col == "":
			col = None
		face = f.split("\"", 2)[1]
		return col, face, size
	except:
		return None, None, None

# Anon id
def _getAnonId(n, ssid):
	"""Gets the anon's id."""
	if n == None: n = "5504"
	try:
		return "".join(list(
			map(lambda x: str(x[0] + x[1])[-1], list(zip(
				list(map(lambda x: int(x), n)),
				list(map(lambda x: int(x), ssid[4:]))
			)))
		))
	except ValueError:
		return "NNNN"

# PM class
class PM:
	"""Manages a connection with Chatango PM."""

	# Init
	def __init__(self, mgr):
		self._auth_re = re.compile(r"auth\.chatango\.com ?= ?([^;]*)", re.IGNORECASE)
		self._connected = False
		self._mgr = mgr
		self._server = "c1.chatango.com"
		self._port = 8081
		self._auid = None
		self._blocklist = set()
		self._contacts = set()
		self._status = dict()
		self._wlock = False
		self._firstCommand = True
		self._pingTask = None
		self._wlockbuf = list()

	# Connections
	async def _connect(self):
		# TODO?
		url = f"wss://{self._server}:{self._port}/"
		self._sock = await websockets.connect(url, origin="https://st.chatango.com")

		self._firstCommand = True
		if not await self._auth():
			return

		asyncio.ensure_future(self.ping())

		self._connected = True

		return await self._main()

	async def _main(self):
		while self._sock.open:
			data = await self._sock.recv()
			await self._feed(data)

	async def _getAuth(self, name, password):
		"""
		Request an auid using name and password.

		@type name: str
		@param name: name
		@type password: str
		@param password: password

		@rtype: str
		@return: auid
		"""

		url = "https://chatango.com/login"
		data = {
			"user_id": name,
			"password": password,
			"storecookie": "on",
			"checkerrors": "yes"
		}

		async with aiohttp.ClientSession() as session:
			async with session.get(url, headers=_HELLO_ALEC, params=data) as res:
				cookie = str(res.cookies["auth.chatango.com"])

				m = self._auth_re.search(cookie)
				if m:
					auth = m.group(1)
					if auth:
						return auth
		return None

	async def _auth(self):
		self._auid = await self._getAuth(self._mgr.name, self._mgr.password)
		if self._auid == None:
			# TODO
			self._sock.close()
			await self._callEvent("onLoginFail")
			self._sock = None
			return False

		await self._sendCommand("tlogin", self._auid, "2")
		await self._setWriteLock(True)

		return True

	async def disconnect(self):
		"""Disconnect the bot from PM"""
		await self._disconnect()
		await self._callEvent("onPMDisconnect")

	async def _disconnect(self):
		# TODO
		self._connected = False
		self._sock.close()
		self._sock = None

	####
	# Feed
	####
	async def _feed(self, food):
		"""
		Feed data to the connection.

		@type data: bytes
		@param data: data to be fed
		"""
		# TODO
		await self._process(food.rstrip("\r\n\0"))

	async def _process(self, data):
		"""
		Process a command string.

		@type data: str
		@param data: the command string
		"""
		await self._callEvent("onRaw", data)
		data = data.split(":")
		cmd, args = data[0], data[1:]
		func = "_rcmd_" + cmd
		if hasattr(self, func):
			await getattr(self, func)(args)

	####
	# Properties
	####
	def _getManager(self): return self._mgr
	def _getContacts(self): return self._contacts
	def _getBlocklist(self): return self._blocklist

	mgr = property(_getManager)
	contacts = property(_getContacts)
	blocklist = property(_getBlocklist)

	####
	# Received Commands
	####
	async def _rcmd_OK(self, args):
		await self._setWriteLock(False)
		await self._sendCommand("wl")
		await self._sendCommand("getblock")
		await self._callEvent("onPMConnect")

	async def _rcmd_wl(self, args):
		self._contacts = set()
		for i in range(len(args) // 4):
			name, last_on, is_on, idle = args[i * 4: i * 4 + 4]
			user = User(name)

			if last_on=="None":
				pass # in case chatango gives a "None" as data argument
			elif not is_on == "on":
				self._status[user] = [int(last_on), False, 0]
			elif idle == '0':
				self._status[user] = [int(last_on), True, 0]
			else:
				self._status[user] = [int(last_on), True, time.time() - int(idle) * 60]

			self._contacts.add(user)

		await self._callEvent("onPMContactlistReceive")

	async def _rcmd_block_list(self, args):
		self._blocklist = set()
		for name in args:
			if name == "":
				continue
			self._blocklist.add(User(name))

	async def _rcmd_idleupdate(self, args):
		user = User(args[0])
		last_on, is_on, idle = self._status[user]
		if args[1] == '1':
			self._status[user] = [last_on, is_on, 0]
		else:
			self._status[user] = [last_on, is_on, time.time()]

	async def _rcmd_track(self, args):
		user = User(args[0])
		if user in self._status:
			last_on = self._status[user][0]
		else:
			last_on = 0
		if args[1] == '0':
			idle = 0
		else:
			idle = time.time() - int(args[1]) * 60
		if args[2] == "online":
			is_on = True
		else:
			is_on = False
		self._status[user] = [last_on, is_on, idle]

	async def _rcmd_DENIED(self, args):
		await self._disconnect()
		await self._callEvent("onLoginFail")

	async def _rcmd_msg(self, args):
		user = User(args[0])
		body = _strip_html(":".join(args[5:]))
		await self._callEvent("onPMMessage", user, body)

	async def _rcmd_msgoff(self, args):
		user = User(args[0])
		body = _strip_html(":".join(args[5:]))
		await self._callEvent("onPMOfflineMessage", user, body)

	async def _rcmd_wlonline(self, args):
		user = User(args[0])
		last_on = float(args[1])
		self._status[user] = [last_on,True,last_on]
		await self._callEvent("onPMContactOnline", user)

	async def _rcmd_wloffline(self, args):
		user = User(args[0])
		last_on = float(args[1])
		self._status[user] = [last_on,False,0]
		await self._callEvent("onPMContactOffline", user)

	async def _rcmd_kickingoff(self, args):
		await self.disconnect()

	async def _rcmd_toofast(self, args):
		await self.disconnect()

	async def _rcmd_unblocked(self, user):
		"""call when successfully unblocked"""
		if user in self._blocklist:
			self._blocklist.remove(user)
			await self._callEvent("onPMUnblock", user)

	# Commands
	async def ping(self):
		"""send a ping"""
		while True:
			await asyncio.sleep(30)
			await self._sendCommand("")
			await self._callEvent("onPMPing")

	async def message(self, user, msg):
		"""send a pm to a user"""
		if msg!=None:
			await self._sendCommand("msg", user.name, msg)

	async def addContact(self, user):
		"""add contact"""
		if user not in self._contacts:
			await self._sendCommand("wladd", user.name)
			self._contacts.add(user)
			await self._callEvent("onPMContactAdd", user)

	async def removeContact(self, user):
		"""remove contact"""
		if user in self._contacts:
			await self._sendCommand("wldelete", user.name)
			self._contacts.remove(user)
			await self._callEvent("onPMContactRemove", user)

	async def block(self, user):
		"""block a person"""
		if user not in self._blocklist:
			await self._sendCommand("block", user.name, user.name, "S")
			self._blocklist.add(user)
			await self._callEvent("onPMBlock", user)

	async def unblock(self, user):
		"""unblock a person"""
		if user in self._blocklist:
			await self._sendCommand("unblock", user.name)

	async def track(self, user):
		"""get and store status of person for future use"""
		await self._sendCommand("track", user.name)

	async def checkOnline(self, user):
		"""return True if online, False if offline, None if unknown"""
		if user in self._status:
			return self._status[user][1]
		else:
			return None

	async def getIdle(self, user):
		"""return last active time, time.time() if isn't idle, 0 if offline, None if unknown"""
		if not user in self._status:
			return None
		if not self._status[user][1]:
			return 0
		if not self._status[user][2]:
			return time.time()
		else:
			return self._status[user][2]

	# Util
	async def _callEvent(self, evt, *args, **kw):
		await getattr(self.mgr, evt)(self, *args, **kw)
		await self.mgr.onEventCalled(self, evt, *args, **kw)

	async def _write(self, data):
		if self._wlock:
			if isinstance(data, list):
				self._wlockbuf.extend(data)
			else:
				self._wlockbuf.append(data)
		else:
			if not isinstance(data, list):
				data = [data]
			for item in data:
				await self._sock.send(item)

	async def _setWriteLock(self, lock):
		self._wlock = lock
		if self._wlock == False:
			await self._write(self._wlockbuf)
			self._wlockbuf = list()

	async def _sendCommand(self, *args):
		"""
		Send a command.

		@type args: [str, str, ...]
		@param args: command and list of arguments
		"""
		if self._firstCommand:
			terminator = "\x00"
			self._firstCommand = False
		else:
			terminator = "\r\n\x00"
		await self._write(":".join(args) + terminator)

	async def getConnections(self):
		return [self]

################################################################
# Room class
################################################################
class Room:
	"""Manages a connection with a Chatango room."""
	####
	# Init
	####
	def __init__(self, room, uid=None, server=None, port=None, mgr=None):
		"""init, don't overwrite"""
		# Basic stuff
		self._name = room
		self._server = server or getServer(room)
		self._port = port or 8081
		self._mgr = mgr

		# Under the hood
		self._connected = False
		self._reconnecting = False
		self._uid = uid or _genUid()
		self._wlockbuf = list()
		self._owner = None
		self._mods = set()
		self._mqueue = dict()
		self._history = list()
		self._userlist = list()
		self._firstCommand = True
		self._connectAmmount = 0
		self._premium = False
		self._userCount = 0
		self._pingTask = None
		self._botname = None
		self._currentname = None
		self._users = dict()
		self._msgs = dict()
		self._wlock = False
		self._silent = False
		self._banlist = dict()
		self._unbanlist = dict()

	####
	# Connect/disconnect
	####
	async def _connect(self):
		"""Connect to the server."""
		# TODO?
		url = f"wss://{self._server}:{self._port}/"
		self._sock = await websockets.connect(url, origin="https://st.chatango.com")

		self._firstCommand = True

		await self._auth()
		asyncio.ensure_future(self.ping())

		if not self._reconnecting:
			self.connected = True

		return await self._main()

	async def _main(self):
		while self._sock.open:
			data = await self._sock.recv()
			await self._feed(data)

	async def reconnect(self):
		"""Reconnect."""
		await self._reconnect()

	async def _reconnect(self):
		"""Reconnect."""
		self._reconnecting = True
		if self.connected:
			await self._disconnect()
		self._uid = _genUid()
		await self._connect()
		self._reconnecting = False

	async def disconnect(self):
		"""Disconnect."""
		await self._disconnect()
		await self._callEvent("onDisconnect")

	async def _disconnect(self):
		"""Disconnect from the server."""
		# TODO
		if not self._reconnecting:
			self.connected = False
		for user in self._userlist:
			await user.clearSessionIds(self)
		self._userlist = list()
		self._sock.close()
		if not self._reconnecting:
			del self.mgr._rooms[self.name]

	async def _auth(self):
		"""Authenticate."""
		# login as name with password
		if self.mgr.name and self.mgr.password:
			await self._sendCommand("bauth", self.name, self._uid, self.mgr.name, self.mgr.password)
			self._currentname = self.mgr.name
		# login as anon
		else:
			await self._sendCommand("bauth", self.name)

		await self._setWriteLock(True)

	####
	# Properties
	####
	def _getName(self): return self._name
	def _getBotName(self):
		if self.mgr.name and self.mgr.password:
			return self.mgr.name
		elif self.mgr.name and self.mgr.password == None:
			return "#" + self.mgr.name
		elif self.mgr.name == None:
			return self._botname
	def _getCurrentname(self): return self._currentname
	def _getManager(self): return self._mgr
	def _getUserlist(self, mode = None, unique = None, memory = None):
		ul = None
		if mode == None: mode = self.mgr._userlistMode
		if unique == None: unique = self.mgr._userlistUnique
		if memory == None: memory = self.mgr._userlistMemory
		if mode == Userlist_Recent:
			ul = map(lambda x: x.user, self._history[-memory:])
		elif mode == Userlist_All:
			ul = self._userlist
		if unique:
			return list(set(ul))
		else:
			return ul
	def _getUserNames(self):
		ul = self.userlist
		return list(map(lambda x: x.name, ul))
	def _getUser(self): return self.mgr.user
	def _getOwner(self): return self._owner
	def _getOwnerName(self): return self._owner.name
	def _getMods(self):
		newset = set()
		for mod in self._mods:
			newset.add(mod)
		return newset
	def _getModNames(self):
		mods = self._getMods()
		return [x.name for x in mods]
	def _getUserCount(self): return self._userCount
	def _getSilent(self): return self._silent
	def _setSilent(self, val): self._silent = val
	def _getBanlist(self): return list(self._banlist.keys())
	def _getUnBanlist(self): return [[record["target"], record["src"]] for record in self._unbanlist.values()]

	name = property(_getName)
	botname = property(_getBotName)
	currentname = property(_getCurrentname)
	mgr = property(_getManager)
	userlist = property(_getUserlist)
	usernames = property(_getUserNames)
	user = property(_getUser)
	owner = property(_getOwner)
	ownername = property(_getOwnerName)
	mods = property(_getMods)
	modnames = property(_getModNames)
	usercount = property(_getUserCount)
	silent = property(_getSilent, _setSilent)
	banlist = property(_getBanlist)
	unbanlist = property(_getUnBanlist)

	####
	# Feed/process
	####
	async def _feed(self, food):
		"""
		Feed data to the connection.

		@type data: bytes
		@param data: data to be fed
		"""
		# TODO
		await self._process(food.rstrip("\r\n\0"))

	async def _process(self, data):
		"""
		Process a command string.
		
		@type data: str
		@param data: the command string
		"""
		await self._callEvent("onRaw", data)
		data = data.split(":")
		cmd, args = data[0], data[1:]
		func = "_rcmd_" + cmd
		if hasattr(self, func):
			await getattr(self, func)(args)

	####
	# Received Commands
	####
	async def _rcmd_ok(self, args):
		# if no name, join room as anon and no password
		if args[2] == "N" and self.mgr.password == None and self.mgr.name == None:
			n = args[4].rsplit('.', 1)[0]
			n = n[-4:]
			aid = args[1][0:8]
			pid = "!anon" + _getAnonId(n, aid)
			self._botname = pid
			self._currentname = pid
			self.user._nameColor = n
		# if got name, join room as name and no password
		elif args[2] == "N" and self.mgr.password == None:
			await self._sendCommand("blogin", self.mgr.name)
			self._currentname = self.mgr.name
		# if got password but fail to login
		elif args[2] != "M": #unsuccesful login
			await self._callEvent("onLoginFail")
			await self.disconnect()
		self._owner = User(args[0])
		self._uid = args[1]
		self._aid = args[1][4:8]
		self._mods = set(map(lambda x: User(x.split(",")[0]), args[6].split(";")))
		self._i_log = list()

	async def _rcmd_denied(self, args):
		await self._disconnect()
		await self._callEvent("onConnectFail")

	async def _rcmd_inited(self, args):
		await self._sendCommand("g_participants", "start")
		await self._sendCommand("getpremium", "1")
		await self.requestBanlist()
		await self.requestUnBanlist()
		if self._connectAmmount == 0:
			await self._callEvent("onConnect")
			for msg in reversed(self._i_log):
				user = msg.user
				await self._callEvent("onHistoryMessage", user, msg)
				await self._addHistory(msg)
			del self._i_log
		else:
			await self._callEvent("onReconnect")
		self._connectAmmount += 1
		await self._setWriteLock(False)

	async def _rcmd_premium(self, args):
		if float(args[1]) > time.time():
			self._premium = True
			if self.user._mbg:
				await self.setBgMode(1)
			if self.user._mrec:
				await self.setRecordingMode(1)
		else:
			self._premium = False

	async def _rcmd_mods(self, args):
		modnames = args
		mods = set(map(lambda x: User(x.split(",")[0]), modnames))
		premods = self._mods
		for user in mods - premods: #modded
			self._mods.add(user)
			await self._callEvent("onModAdd", user)
		for user in premods - mods: #demodded
			self._mods.remove(user)
			await self._callEvent("onModRemove", user)
		await self._callEvent("onModChange")

	async def _rcmd_b(self, args):
		mtime = float(args[0])
		puid = args[3]
		ip = args[6]
		name = args[1]
		rawmsg = ":".join(args[9:])
		msg, n, f = _clean_message(rawmsg)
		if name == "":
			nameColor = None
			name = "#" + args[2]
			if name == "#":
				name = "!anon" + _getAnonId(n, puid)
		else:
			nameColor = None
			if n:
				nameColor = _parseNameColor(n)
		i = args[5]
		unid = args[4]
		user = User(name)
		#Create an anonymous message and queue it because msgid is unknown.
		if f:
			fontColor, fontFace, fontSize = _parseFont(f)
		else:
			fontColor, fontFace, fontSize = None, None, None
		msg = Message(
			time = mtime,
			user = user,
			body = msg,
			raw = rawmsg,
			ip = ip,
			nameColor = nameColor,
			fontColor = fontColor,
			fontFace = fontFace,
			fontSize = fontSize,
			unid = unid,
			puid = puid,
			room = self
		)
		self._mqueue[i] = msg

	async def _rcmd_u(self, args):
		temp = Struct(**self._mqueue)
		if hasattr(temp, args[0]):
			msg = getattr(temp, args[0])
			if msg.user != self.user:
				msg.user._fontColor = msg.fontColor
				msg.user._fontFace = msg.fontFace
				msg.user._fontSize = msg.fontSize
				msg.user._nameColor = msg.nameColor
			del self._mqueue[args[0]]
			await msg.attach(self, args[1])
			await self._addHistory(msg)
			await self._callEvent("onMessage", msg.user, msg)

	async def _rcmd_i(self, args):
		mtime = float(args[0])
		puid = args[3]
		ip = args[6]
		name = args[1]
		rawmsg = ":".join(args[9:])
		msg, n, f = _clean_message(rawmsg)
		if name == "":
			nameColor = None
			name = "#" + args[2]
			if name == "#":
				name = "!anon" + _getAnonId(n, puid)
		else:
			nameColor = None
			if n:
				nameColor = _parseNameColor(n)
		i = args[5]
		unid = args[4]
		user = User(name)
		#Create an anonymous message and queue it because msgid is unknown.
		if f:
			fontColor, fontFace, fontSize = _parseFont(f)
		else:
			fontColor, fontFace, fontSize = None, None, None
		msg = Message(
			time = mtime,
			user = user,
			body = msg,
			raw = rawmsg,
			ip = ip,
			nameColor = nameColor,
			fontColor = fontColor,
			fontFace = fontFace,
			fontSize = fontSize,
			unid = unid,
			puid = puid,
			room = self
		)
		self._i_log.append(msg)

	async def _rcmd_g_participants(self, args):
		args = ":".join(args)
		args = args.split(";")
		for data in args:
			data = data.split(":")
			name = data[3].lower()
			if name == "none": continue
			user = User(
				name = name,
				room = self
			)
			await user.addSessionId(self, data[0])
			self._userlist.append(user)

	async def _rcmd_participant(self, args):
		name = args[3].lower()
		if name == "none": return
		user = User(name)
		puid = args[2]

		if args[0] == "0": #leave
			await user.removeSessionId(self, args[1])
			self._userlist.remove(user)
			if user not in self._userlist or not self.mgr._userlistEventUnique:
				await self._callEvent("onLeave", user, puid)
		else: #join
			await user.addSessionId(self, args[1])
			doEvent = False
			if user not in self._userlist:
				doEvent = True
			self._userlist.append(user)
			if doEvent or not self.mgr._userlistEventUnique:
				await self._callEvent("onJoin", user, puid)

	async def _rcmd_show_fw(self, args):
		await self._callEvent("onFloodWarning")

	async def _rcmd_show_tb(self, args):
		await self._callEvent("onFloodBan")

	async def _rcmd_tb(self, args):
		await self._callEvent("onFloodBanRepeat")

	async def _rcmd_delete(self, args):
		msg = self._msgs.get(args[0])
		if msg:
			if msg in self._history:
				self._history.remove(msg)
				await self._callEvent("onMessageDelete", msg.user, msg)
				await msg.detach()

	async def _rcmd_deleteall(self, args):
		for msgid in args:
			await self._rcmd_delete([msgid])

	async def _rcmd_n(self, args):
		self._userCount = int(args[0], 16)
		await self._callEvent("onUserCountChange")

	async def _rcmd_blocklist(self, args):
		self._banlist = dict()
		sections = ":".join(args).split(";")
		for section in sections:
			params = section.split(":")
			if len(params) != 5: continue
			if params[2] == "": continue
			user = User(params[2])
			self._banlist[user] = {
				"unid":params[0],
				"ip":params[1],
				"target":user,
				"time":float(params[3]),
				"src":User(params[4])
			}
		await self._callEvent("onBanlistUpdate")

	async def _rcmd_unblocklist(self, args):
		self._unbanlist = dict()
		sections = ":".join(args).split(";")
		for section in sections:
			params = section.split(":")
			if len(params) != 5: continue
			if params[2] == "": continue
			user = User(params[2])
			self._unbanlist[user] = {
				"unid":params[0],
				"ip":params[1],
				"target":user,
				"time":float(params[3]),
				"src":User(params[4])
			}
		await self._callEvent("onUnBanlistUpdate")

	async def _rcmd_blocked(self, args):
		if args[2] == "": return
		target = User(args[2])
		user = User(args[3])
		self._banlist[target] = {"unid":args[0], "ip":args[1], "target":target, "time":float(args[4]), "src":user}
		await self._callEvent("onBan", user, target)

	async def _rcmd_unblocked(self, args):
		if args[2] == "": return
		target = User(args[2])
		user=User(args[3])
		del self._banlist[target]
		self._unbanlist[user] = {"unid":args[0], "ip":args[1], "target":target, "time":float(args[4]), "src":user}
		await self._callEvent("onUnban", user, target)

	####
	# Commands
	####
	async def login(self, NAME, PASS = None):
		"""login as a user or set a name in room"""
		if PASS:
			await self._sendCommand("blogin", NAME, PASS)
		else:
			await self._sendCommand("blogin", NAME)
		self._currentname = NAME

	async def logout(self):
		"""logout of user in a room"""
		await self._sendCommand("blogout")
		self._currentname = self._botname

	async def ping(self):
		"""Send a ping."""
		while True:
			await asyncio.sleep(30)
			await self._sendCommand("")
			await self._callEvent("onPing")

	async def rawMessage(self, msg):
		"""
		Send a message without n and f tags.

		@type msg: str
		@param msg: message
		"""
		if not self._silent:
			await self._sendCommand("bm", "porn", "0", msg)

	async def message(self, msg, html=False):
		"""
		Send a message. (Use "\n" for new line)

		@type msg: str
		@param msg: message
		"""
		if msg==None:
			return
		msg = msg.rstrip()
		if not html:
			msg = msg.replace("<", "&lt;").replace(">", "&gt;")
		if len(msg) > self.mgr._maxLength:
			if self.mgr._tooBigMessage == BigMessage_Cut:
				await self.message(msg[:self.mgr._maxLength], html = html)
			elif self.mgr._tooBigMessage == BigMessage_Multiple:
				while len(msg) > 0:
					sect = msg[:self.mgr._maxLength]
					msg = msg[self.mgr._maxLength:]
					await self.message(sect, html=html)
			return
		n_tag = f"<n{self.user.nameColor}/>"
		if self._currentname != None and not self._currentname.startswith("!anon"):
			f_size = str(self.user.fontSize).rjust(2, "0")
			msg = f'{n_tag}<f x{f_size}{self.user.fontColor}="{self.user.fontFace}">{msg}'
		msg = msg.replace("\n", "\r")
		msg = msg.replace("~","&#126;")

		await self.rawMessage(msg)

	async def setBgMode(self, mode):
		"""turn on/off bg"""
		await self._sendCommand("msgbg", str(mode))

	async def setRecordingMode(self, mode):
		"""turn on/off rcecording"""
		await self._sendCommand("msgmedia", str(mode))

	async def addMod(self, user):
		"""
		Add a moderator.

		@type user: User
		@param user: User to mod.
		"""
		if await self.getLevel(User(self.currentname)) == 2:
			await self._sendCommand("addmod", user.name)

	async def removeMod(self, user):
		"""
		Remove a moderator.

		@type user: User
		@param user: User to demod.
		"""
		if await self.getLevel(User(self.currentname)) == 2:
			await self._sendCommand("removemod", user.name)

	async def flag(self, message):
		"""
		Flag a message.

		@type message: Message
		@param message: message to flag
		"""
		await self._sendCommand("g_flag", message.msgid)

	async def flagUser(self, user):
		"""
		Flag a user.

		@type user: User
		@param user: user to flag

		@rtype: bool
		@return: whether a message to flag was found
		"""
		msg = await self.getLastMessage(user)
		if msg:
			await self.flag(msg)
			return True
		return False

	async def deleteMessage(self, message):
		"""
		Delete a message. (Moderator only)

		@type message: Message
		@param message: message to delete
		"""
		if await self.getLevel(self.user) > 0:
			await self._sendCommand("delmsg", message.msgid)

	async def deleteUser(self, user):
		"""
		Delete a message. (Moderator only)

		@type message: User
		@param message: delete user's last message
		"""
		if await self.getLevel(self.user) > 0:
			msg = await self.getLastMessage(user)
			if msg:
				await self._sendCommand("delmsg", msg.msgid)
				return True
		return False

	async def delete(self, message):
		"""
		compatibility wrapper for deleteMessage
		"""
		#print("[obsolete] the delete function is obsolete, please use deleteMessage")
		return await self.deleteMessage(message)

	async def rawClearUser(self, unid, ip, user):
		await self._sendCommand("delallmsg", unid, ip, user)

	async def clearUser(self, user):
		"""
		Clear all of a user's messages. (Moderator only)

		@type user: User
		@param user: user to delete messages of

		@rtype: bool
		@return: whether a message to delete was found
		"""
		if await self.getLevel(self.user) > 0:
			msg = await self.getLastMessage(user)
			if msg:
				if msg.user.name[0] in ["!", "#"]:
					# anon or temp
					await self.rawClearUser(msg.unid, msg.ip,"")
				else:
					await self.rawClearUser(msg.unid,msg.ip,msg.user.name)
				return True
		return False

	async def clearall(self):
		"""Clear all messages. (Owner only)"""
		if await self.getLevel(self.user) == 2:
			await self._sendCommand("clearall")

	async def rawBan(self, name, ip, unid):
		"""
		Execute the block command using specified arguments.
		(For advanced usage)

		@type name: str
		@param name: name
		@type ip: str
		@param ip: ip address
		@type unid: str
		@param unid: unid
		"""
		await self._sendCommand("block", unid, ip, name)

	async def ban(self, msg):
		"""
		Ban a message's sender. (Moderator only)

		@type message: Message
		@param message: message to ban sender of
		"""
		if await self.getLevel(self.user) > 0:
			await self.rawBan(msg.user.name, msg.ip, msg.unid)

	async def banUser(self, user):
		"""
		Ban a user. (Moderator only)

		@type user: User
		@param user: user to ban

		@rtype: bool
		@return: whether a message to ban the user was found
		"""
		msg = await self.getLastMessage(user)
		if msg:
			await self.ban(msg)
			return True
		return False

	async def requestBanlist(self):
		"""Request an updated banlist."""
		await self._sendCommand("blocklist", "block", "", "next", "500")

	async def requestUnBanlist(self):
		"""Request an updated banlist."""
		await self._sendCommand("blocklist", "unblock", "", "next", "500")

	async def rawUnban(self, name, ip, unid):
		"""
		Execute the unblock command using specified arguments.
		(For advanced usage)

		@type name: str
		@param name: name
		@type ip: str
		@param ip: ip address
		@type unid: str
		@param unid: unid
		"""
		await self._sendCommand("removeblock", unid, ip, name)

	async def unban(self, user):
		"""
		Unban a user. (Moderator only)

		@type user: User
		@param user: user to unban

		@rtype: bool
		@return: whether it succeeded
		"""
		rec = await self._getBanRecord(user)
		if rec:
			await self.rawUnban(rec["target"].name, rec["ip"], rec["unid"])
			return True
		else:
			return False

	####
	# Util
	####
	async def _getBanRecord(self, user):
		if user in self._banlist:
			return await self._banlist[user]
		return None

	async def _callEvent(self, evt, *args, **kw):
		handler = getattr(self.mgr, evt, None)
		if handler:
			await handler(self, *args, **kw)
			await self.mgr.onEventCalled(self, evt, *args, **kw)

	async def _write(self, data):
		# TODO
		if self._wlock:
			if isinstance(data, list):
				self._wlockbuf.extend(data)
			else:
				self._wlockbuf.append(data)
		else:
			if not isinstance(data, list):
				data = [data]
			for item in data:
				await self._sock.send(item)

	async def _setWriteLock(self, lock):
		# TODO
		self._wlock = lock
		if self._wlock == False:
			await self._write(self._wlockbuf)
			self._wlockbuf = list()

	async def _sendCommand(self, *args):
		"""
		Send a command.

		@type args: [str, str, ...]
		@param args: command and list of arguments
		"""
		# TODO
		if self._firstCommand:
			terminator = "\x00"
			self._firstCommand = False
		else:
			terminator = "\r\n\x00"
		await self._write(":".join(args) + terminator)

	async def getLevel(self, user):
		"""get the level of user in a room"""
		if user == self._owner:
			return 2
		if user.name in self.modnames:
			return 1
		return 0

	async def getLastMessage(self, user = None):
		"""get last message said by user in a room"""
		if user:
			try:
				i = 1
				while True:
					msg = self._history[-i]
					if msg.user == user:
						return msg
					i += 1
			except IndexError:
				return None
		else:
			try:
				return self._history[-1]
			except IndexError:
				return None
		return None

	async def findUser(self, name):
		"""check if user is in the room
		
		return User(name) if name in room else None"""
		name = name.lower()
		ul = await self._getUserlist()
		udi = dict(zip([u.name for u in ul], ul))
		cname = None
		for n in udi.keys():
			if name in n:
				if cname: return None #ambiguous!!
				cname = n
		if cname: return udi[cname]
		else: return None

	####
	# History
	####
	async def _addHistory(self, msg):
		"""
		Add a message to history.

		@type msg: Message
		@param msg: message
		"""
		self._history.append(msg)
		if len(self._history) > self.mgr._maxHistoryLength:
			rest, self._history = self._history[:-self.mgr._maxHistoryLength], self._history[-self.mgr._maxHistoryLength:]
			for msg in rest: await msg.detach()

################################################################
# RoomManager class
################################################################
class RoomManager:
	"""Class that manages multiple connections."""
	####
	# Config
	####
	_Room = Room
	_PM = PM
	_PMHost = "c1.chatango.com"
	_PMPort = 8080
	_TimerResolution = 0.2 #at least x times per second
	_pingDelay = 30
	_userlistMode = Userlist_Recent
	_userlistUnique = True
	_userlistMemory = 50
	_userlistEventUnique = False
	_tooBigMessage = BigMessage_Multiple
	_maxLength = 1800
	_maxHistoryLength = 150

	####
	# Init
	####
	def __init__(self, name_=None, password=None, pm=True):
		self._name = name_
		self._password = password
		self._running = False
		self._rooms = dict()
		self._pm = None

		if pm and self._password:
			self._pm = self._PM(mgr=self)

	####
	# Join/leave
	####
	async def joinRoom(self, room):
		"""
		Join a room or return None if already joined.

		@type room: str
		@param room: room to join

		@rtype: Room or None
		@return: True or nothing
		"""
		room = room.lower()
		con = self._Room(room, mgr=self)
		asyncio.ensure_future(con._connect())
		self._rooms[room] = con

	async def leaveRoom(self, room):
		"""
		Leave a room.

		@type room: str
		@param room: room to leave
		"""
		room = room.lower()
		if room in self._rooms:
			await room.disconnect()
			self._rooms.remove(room)

	async def getRoom(self, room):
		"""
		Get room with a name, or None if not connected to this room.

		@type room: str
		@param room: room

		@rtype: Room
		@return: the room
		"""
		room = room.lower()
		return self._rooms.get(room.lower())

	####
	# Properties
	####
	def _getUser(self): return User(self._name)
	def _getName(self): return self._name
	def _getPassword(self): return self._password
	def _getRooms(self): return set(self._rooms.values())
	def _getRoomNames(self): return set(self._rooms.keys())
	def _getPM(self): return self._pm

	user = property(_getUser)
	name = property(_getName)
	password = property(_getPassword)
	rooms = property(_getRooms)
	roomnames = property(_getRoomNames)
	pm = property(_getPM)

	####
	# Virtual methods
	####
	async def onInit(self):
		"""Called on init."""
		pass

	async def onConnect(self, room):
		"""
		Called when connected to the room.
		
		@type room: Room
		@param room: room where the event occured
		"""
		pass

	async def onReconnect(self, room):
		"""
		Called when reconnected to the room.
		
		@type room: Room
		@param room: room where the event occured
		"""
		pass

	async def onConnectFail(self, room):
		"""
		Called when the connection failed.
		
		@type room: Room
		@param room: room where the event occured
		"""
		pass

	async def onDisconnect(self, room):
		"""
		Called when the client gets disconnected.
		
		@type room: Room
		@param room: room where the event occured
		"""
		pass

	async def onLoginFail(self, room):
		"""
		Called on login failure, disconnects after.
		
		@type room: Room
		@param room: room where the event occured
		"""
		pass

	async def onFloodBan(self, room):
		"""
		Called when either flood banned or flagged.
		
		@type room: Room
		@param room: room where the event occured
		"""
		pass

	async def onFloodBanRepeat(self, room):
		"""
		Called when trying to send something when floodbanned.

		@type room: Room
		@param room: room where the event occured
		"""
		pass

	async def onFloodWarning(self, room):
		"""
		Called when an overflow warning gets received.

		@type room: Room
		@param room: room where the event occured
		"""
		pass

	async def onMessageDelete(self, room, user, message):
		"""
		Called when a message gets deleted.

		@type room: Room
		@param room: room where the event occured
		@type user: User
		@param user: owner of deleted message
		@type message: Message
		@param message: message that got deleted
		"""
		pass

	async def onModChange(self, room):
		"""
		Called when the moderator list changes.

		@type room: Room
		@param room: room where the event occured
		"""
		pass

	async def onModAdd(self, room, user):
		"""
		Called when a moderator gets added.

		@type room: Room
		@param room: room where the event occured
		"""
		pass

	async def onModRemove(self, room, user):
		"""
		Called when a moderator gets removed.

		@type room: Room
		@param room: room where the event occured
		"""
		pass

	async def onMessage(self, room, user, message):
		"""
		Called when a message gets received.

		@type room: Room
		@param room: room where the event occured
		@type user: User
		@param user: owner of message
		@type message: Message
		@param message: received message
		"""
		pass

	async def onHistoryMessage(self, room, user, message):
		"""
		Called when a message gets received from history.

		@type room: Room
		@param room: room where the event occured
		@type user: User
		@param user: owner of message
		@type message: Message
		@param message: the message that got added
		"""
		pass

	async def onJoin(self, room, user, puid):
		"""
		Called when a user joins. Anonymous users get ignored here.

		@type room: Room
		@param room: room where the event occured
		@type user: User
		@param user: the user that has joined
		@type puid: str
		@param puid: the personal unique id for the user
		"""
		pass

	async def onLeave(self, room, user, puid):
		"""
		Called when a user leaves. Anonymous users get ignored here.

		@type room: Room
		@param room: room where the event occured
		@type user: User
		@param user: the user that has left
		@type puid: str
		@param puid: the personal unique id for the user
		"""
		pass

	async def onRaw(self, room, raw):
		"""
		Called before any command parsing occurs.

		@type room: Room
		@param room: room where the event occured
		@type raw: str
		@param raw: raw command data
		"""
		pass

	async def onPing(self, room):
		"""
		Called when a ping gets sent.

		@type room: Room
		@param room: room where the event occured
		"""
		pass

	async def onUserCountChange(self, room):
		"""
		Called when the user count changes.

		@type room: Room
		@param room: room where the event occured
		"""
		pass

	async def onBan(self, room, user, target):
		"""
		Called when a user gets banned.

		@type room: Room
		@param room: room where the event occured
		@type user: User
		@param user: user that banned someone
		@type target: User
		@param target: user that got banned
		"""
		pass
 
	async def onUnban(self, room, user, target):
		"""
		Called when a user gets unbanned.

		@type room: Room
		@param room: room where the event occured
		@type user: User
		@param user: user that unbanned someone
		@type target: User
		@param target: user that got unbanned
		"""
		pass

	async def onBanlistUpdate(self, room):
		"""
		Called when a banlist gets updated.

		@type room: Room
		@param room: room where the event occured
		"""
		pass

	async def onUnBanlistUpdate(self, room):
		"""
		Called when a unbanlist gets updated.

		@type room: Room
		@param room: room where the event occured
		"""
		pass

	async def onPMConnect(self, pm):
		"""
		Called when connected to the pm
		
		@type pm: PM
		@param pm: the pm
		"""
		pass

	async def onAnonPMDisconnect(self, pm, user):
		"""
		Called when disconnected from the pm
		
		@type pm: PM
		@param pm: the pm
		"""
		pass

	async def onPMDisconnect(self, pm):
		"""
		Called when disconnected from the pm
		
		@type pm: PM
		@param pm: the pm
		"""
		pass

	async def onPMPing(self, pm):
		"""
		Called when sending a ping to the pm
		
		@type pm: PM
		@param pm: the pm
		"""
		pass

	async def onPMMessage(self, pm, user, body):
		"""
		Called when a message is received
		
		@type pm: PM
		@param pm: the pm
		@type user: User
		@param user: owner of message
		@type message: Message
		@param message: received message
		"""
		pass

	async def onPMOfflineMessage(self, pm, user, body):
		"""
		Called when connected if a message is received while offline
		
		@type pm: PM
		@param pm: the pm
		@type user: User
		@param user: owner of message
		@type message: Message
		@param message: received message
		"""
		pass

	async def onPMContactlistReceive(self, pm):
		"""
		Called when the contact list is received
		
		@type pm: PM
		@param pm: the pm
		"""
		pass

	async def onPMBlocklistReceive(self, pm):
		"""
		Called when the block list is received
		
		@type pm: PM
		@param pm: the pm
		"""
		pass

	async def onPMContactAdd(self, pm, user):
		"""
		Called when the contact added message is received
		
		@type pm: PM
		@param pm: the pm
		@type user: User
		@param user: the user that gotten added
		"""
		pass

	async def onPMContactRemove(self, pm, user):
		"""
		Called when the contact remove message is received
		
		@type pm: PM
		@param pm: the pm
		@type user: User
		@param user: the user that gotten remove
		"""
		pass

	async def onPMBlock(self, pm, user):
		"""
		Called when successfully block a user
		
		@type pm: PM
		@param pm: the pm
		@type user: User
		@param user: the user that gotten block
		"""
		pass

	async def onPMUnblock(self, pm, user):
		"""
		Called when successfully unblock a user
		
		@type pm: PM
		@param pm: the pm
		@type user: User
		@param user: the user that gotten unblock
		"""
		pass

	async def onPMContactOnline(self, pm, user):
		"""
		Called when a user from the contact come online
		
		@type pm: PM
		@param pm: the pm
		@type user: User
		@param user: the user that came online
		"""
		pass

	async def onPMContactOffline(self, pm, user):
		"""
		Called when a user from the contact go offline
		
		@type pm: PM
		@param pm: the pm
		@type user: User
		@param user: the user that went offline
		"""
		pass

	async def onEventCalled(self, room, evt, *args, **kw):
		"""
		Called on every room-based event.

		@type room: Room
		@param room: room where the event occured
		@type evt: str
		@param evt: the event
		"""
		pass

	# Util
	async def getConnections(self):
		# TODO
		li = self._rooms.values()
		if self._pm:
			li.extend(await self._pm.getConnections())
		return [c for c in li if c.connected]

	# Main
	async def main(self):
		await self.onInit()

		self._running = True

		await self._pm._connect()

	@classmethod
	async def easy_start(cls, rooms=None, name=None, password=None, pm=True):
		"""
		Prompts the user for missing info, then starts.

		@type rooms: list
		@param room: rooms to join
		@type name: str
		@param name: name to join as ("" = None, None = unspecified)
		@type password: str
		@param password: password to join with ("" = None, None = unspecified)
		"""
		if not rooms: rooms = str(input("Room names separated by semicolons: ")).split(";")
		if len(rooms) == 1 and rooms[0] == "":
			rooms = []
		if not name:
			name = str(input("User name: "))
		if name == "":
			name = None
		if not password:
			password = str(getpass.getpass("User password: "))
		if password == "":
			password = None

		self = cls(name, password, pm = pm)

		for room in rooms:
			await self.joinRoom(room)

		await self.main()

	async def stop(self):
		for conn in list(self._rooms.values()):
			await conn.disconnect()
		self._running = False

	####
	# Commands
	####
	async def enableBg(self):
		"""Enable background if available."""
		self.user._mbg = True
		for room in self.rooms:
			await room.setBgMode(1)

	async def disableBg(self):
		"""Disable background."""
		self.user._mbg = False
		for room in self.rooms:
			await room.setBgMode(0)

	async def enableRecording(self):
		"""Enable recording if available."""
		self.user._mrec = True
		for room in self.rooms:
			await room.setRecordingMode(1)

	async def disableRecording(self):
		"""Disable recording."""
		self.user._mrec = False
		for room in self.rooms:
			await room.setRecordingMode(0)

	async def setNameColor(self, color3x):
		"""
		Set name color.

		@type color3x: str
		@param color3x: a 3-char RGB hex code for the color
		"""
		self.user._nameColor = color3x

	async def setFontColor(self, color3x):
		"""
		Set font color.

		@type color3x: str
		@param color3x: a 3-char RGB hex code for the color
		"""
		self.user._fontColor = color3x

	async def setFontFace(self, face):
		"""
		Set font face/family.

		@type face: str
		@param face: the font face
		"""
		self.user._fontFace = face

	async def setFontSize(self, size):
		"""
		Set font size.

		@type size: int
		@param size: the font size (limited: 9 to 22)
		"""
		if size < 9: size = 9
		if size > 22: size = 22
		self.user._fontSize = size

################################################################
# User class (well, yeah, I lied, it's actually _User)
################################################################
_users = dict()
def User(name, *args, **kw):
	if name == None: name = ""
	name = name.lower()
	user = _users.get(name)
	if not user:
		user = _User(name = name, *args, **kw)
		_users[name] = user
	return user

class _User:
	"""Class that represents a user."""
	####
	# Init
	####
	def __init__(self, name, **kw):
		self._name = name.lower()
		self._sids = dict()
		self._msgs = list()
		self._nameColor = "000"
		self._fontSize = 12
		self._fontFace = "0"
		self._fontColor = "000"
		self._mbg = False
		self._mrec = False
		for attr, val in kw.items():
			if val == None: continue
			setattr(self, "_" + attr, val)

	####
	# Properties
	####
	def _getName(self): return self._name
	def _getSessionIds(self, room = None):
		if room:
			return self._sids.get(room, set())
		else:
			return set.union(*self._sids.values())
	def _getRooms(self): return self._sids.keys()
	def _getRoomNames(self): return [room.name for room in self._getRooms()]
	def _getFontColor(self): return self._fontColor
	def _getFontFace(self): return self._fontFace
	def _getFontSize(self): return self._fontSize
	def _getNameColor(self): return self._nameColor

	name = property(_getName)
	sessionids = property(_getSessionIds)
	rooms = property(_getRooms)
	roomnames = property(_getRoomNames)
	fontColor = property(_getFontColor)
	fontFace = property(_getFontFace)
	fontSize = property(_getFontSize)
	nameColor = property(_getNameColor)

	####
	# Util
	####
	async def addSessionId(self, room, sid):
		if room not in self._sids:
			self._sids[room] = set()
		self._sids[room].add(sid)

	async def removeSessionId(self, room, sid):
		try:
			self._sids[room].remove(sid)
			if len(self._sids[room]) == 0:
				del self._sids[room]
		except KeyError:
			pass

	async def clearSessionIds(self, room):
		try:
			del self._sids[room]
		except KeyError:
			pass

	async def hasSessionId(self, room, sid):
		try:
			if sid in self._sids[room]:
				return True
			else:
				return False
		except KeyError:
			return False

	####
	# Repr
	####
	def __repr__(self):
		return "<User: %s>" %(self.name)

################################################################
# Message class
################################################################
class Message:
	"""Class that represents a message."""
	####
	# Attach/detach
	####
	async def attach(self, room, msgid):
		"""
		Attach the Message to a message id.

		@type msgid: str
		@param msgid: message id
		"""
		if self._msgid == None:
			self._room = room
			self._msgid = msgid
			self._room._msgs[msgid] = self

	async def detach(self):
		"""Detach the Message."""
		if self._msgid != None and self._msgid in self._room._msgs:
			del self._room._msgs[self._msgid]
			self._msgid = None

	async def delete(self):
		await self._room.deleteMessage(self)

	####
	# Init
	####
	def __init__(self, **kw):
		"""init, don't overwrite"""
		self._msgid = None
		self._time = None
		self._user = None
		self._body = None
		self._room = None
		self._raw = ""
		self._ip = None
		self._unid = ""
		self._puid = ""
		self._uid = ""
		self._nameColor = "000"
		self._fontSize = 12
		self._fontFace = "0"
		self._fontColor = "000"
		for attr, val in kw.items():
			if val == None: continue
			setattr(self, "_" + attr, val)

	####
	# Properties
	####
	def _getId(self): return self._msgid
	def _getTime(self): return self._time
	def _getUser(self): return self._user
	def _getBody(self): return self._body
	def _getIP(self): return self._ip
	def _getFontColor(self): return self._fontColor
	def _getFontFace(self): return self._fontFace
	def _getFontSize(self): return self._fontSize
	def _getNameColor(self): return self._nameColor
	def _getRoom(self): return self._room
	def _getRaw(self): return self._raw
	def _getUnid(self): return self._unid
	def _getPuid(self): return self._puid

	msgid = property(_getId)
	time = property(_getTime)
	user = property(_getUser)
	body = property(_getBody)
	room = property(_getRoom)
	ip = property(_getIP)
	fontColor = property(_getFontColor)
	fontFace = property(_getFontFace)
	fontSize = property(_getFontSize)
	raw = property(_getRaw)
	nameColor = property(_getNameColor)
	unid = property(_getUnid)
	puid = property(_getPuid)
	uid = property(_getPuid) # other library use uid so we create an alias
